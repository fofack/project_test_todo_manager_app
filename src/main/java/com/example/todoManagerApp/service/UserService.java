package com.example.todoManagerApp.service;

import com.example.todoManagerApp.exception.ResourceNotFoundException;
import com.example.todoManagerApp.models.User;
import com.example.todoManagerApp.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final MongoTemplate mongoTemplate;

    public List<User> findAllUser(){
        return userRepository.findAll();
    }

    public User create(User user){
        return userRepository.save(user);
    }

    public Optional<User> findUser(String id){
        return userRepository.findById(id);
    }

    public User updateUser(String userId, User user) throws ResourceNotFoundException {
        User newUser = mongoTemplate.findOne(Query.query(Criteria.where("id").is(userId)), User.class);
        if(newUser == null){
            throw new ResourceNotFoundException("RESOURCE NOT_FOUND" + userId);
        }
        newUser.setName(user.getName());
        newUser.setEmail(user.getEmail());
        return userRepository.save(newUser);
    }

    public Void deleteUser(String id){
        userRepository.deleteById(id);
        return null;
    }
}
