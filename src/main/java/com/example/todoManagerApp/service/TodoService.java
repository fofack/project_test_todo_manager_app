package com.example.todoManagerApp.service;

import com.example.todoManagerApp.exception.ResourceNotFoundException;
import com.example.todoManagerApp.models.Todo;
import com.example.todoManagerApp.repository.TodoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class TodoService {

    private final TodoRepository todoRepository;
    private final MongoTemplate mongoTemplate;

    public List<Todo> findAllTodo(){
        return todoRepository.findAll();
    }

    public Todo create(Todo todo){
        return todoRepository.save(todo);
    }

    public Optional<Todo> findTodo(String id){
        return todoRepository.findById(id);
    }

    public Todo updateTodo(String todoId, Todo todo) throws ResourceNotFoundException {
        Todo newTodo = mongoTemplate.findOne(Query.query(Criteria.where("id").is(todoId)), Todo.class);
        if(newTodo == null){
            throw new ResourceNotFoundException("RESOURCE NOT_FOUND" + todoId);
        }
        newTodo.setName(todo.getName());
        newTodo.setDescription(todo.getDescription());
        newTodo.setEndDate(todo.getEndDate());
        newTodo.setCompleted(todo.getCompleted());
        return todoRepository.save(newTodo);
    }

    public Void deleteTodo(String id){
        todoRepository.deleteById(id);
        return null;
    }

    public void terminate(String id) throws ResourceNotFoundException {
        Todo newTodo = mongoTemplate.findOne(Query.query(Criteria.where("id").is(id)), Todo.class);
        if(newTodo == null){
            throw new ResourceNotFoundException("RESOURCE NOT_FOUND" + id);
        }
        newTodo.setCompleted(true);
        todoRepository.save(newTodo);
        return;
    }

    public List<Todo> getAllTodoForUser(String userId){
        List<Todo> todo = todoRepository.getAllTodoUser(userId);
        return todo;
    }
}
