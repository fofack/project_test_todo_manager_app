package com.example.todoManagerApp.repository;

import com.example.todoManagerApp.models.Todo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoRepository extends MongoRepository<Todo, String> {

    @Query("{'userId' : }")
    public List<Todo> getAllTodoUser(@Param("id") String id);
}
