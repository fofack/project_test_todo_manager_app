package com.example.todoManagerApp.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import java.util.Date;

@Getter
@Setter
@Document(collection = "todos")
public class Todo {

    @Id
    private String id;
    private String name;
    private Boolean completed;
    private String description;

    @Field(name = "end_date")
    private Date endDate;

    private String userId;
}
