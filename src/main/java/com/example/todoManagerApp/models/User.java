package com.example.todoManagerApp.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter
@Setter
@Data
@Document(collection = "users")
public class User {
    @Id
    private String id;
    private String name;
    private String email;
}
