package com.example.todoManagerApp.controller;

import com.example.todoManagerApp.exception.ResourceNotFoundException;
import com.example.todoManagerApp.models.Todo;
import com.example.todoManagerApp.models.User;
import com.example.todoManagerApp.service.TodoService;
import com.example.todoManagerApp.service.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/todos")
public class TodoController {

    private final TodoService todoService;

    @GetMapping("/")
    public ResponseEntity<List> index(){
        return ResponseEntity.ok(todoService.findAllTodo());
    }

    @PostMapping("/")
    public ResponseEntity<Todo> create(@RequestBody Todo todo){
        return ResponseEntity.ok(todoService.create(todo));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Todo> update(@PathVariable String id , @RequestBody Todo todo) throws ResourceNotFoundException {
        return ResponseEntity.ok(todoService.updateTodo(id,todo));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Todo>> show(@PathVariable String id){
        return ResponseEntity.ok(todoService.findTodo(id));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> destroy(@PathVariable String id){
        return ResponseEntity.ok(todoService.deleteTodo(id));
    }

    @PutMapping("/terminate/{id}")
    public ResponseEntity<?> updateCompleted(@PathVariable String id) throws ResourceNotFoundException {
        todoService.terminate(id);
        return ResponseEntity.ok().body(null);
    }

    @GetMapping("/todo/user/{userId}")
    public ResponseEntity<List<Todo>> getAllTodoUser(String id){
        return ResponseEntity.ok(todoService.getAllTodoForUser(id));
    }
}
