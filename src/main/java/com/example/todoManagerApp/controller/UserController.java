package com.example.todoManagerApp.controller;

import com.example.todoManagerApp.exception.ResourceNotFoundException;
import com.example.todoManagerApp.models.User;
import com.example.todoManagerApp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @GetMapping("/")
    public ResponseEntity<List> index(){
        return ResponseEntity.ok(userService.findAllUser());
    }

    @PostMapping("/")
    public ResponseEntity<User> create(@RequestBody User user){
        return ResponseEntity.ok(userService.create(user));
    }

    @PutMapping("/{id}")
    public ResponseEntity<User> update(@PathVariable String id ,@RequestBody User user) throws ResourceNotFoundException {
        return ResponseEntity.ok(userService.updateUser(id, user));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<User>> show(@PathVariable String id){
        return ResponseEntity.ok(userService.findUser(id));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> destroy(@PathVariable String id){
        return ResponseEntity.ok(userService.deleteUser(id));
    }
}
